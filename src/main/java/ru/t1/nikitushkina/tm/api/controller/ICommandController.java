package ru.t1.nikitushkina.tm.api.controller;

public interface ICommandController {

    void showAbout();

    void showErrorArgument();

    void showErrorCommand();

    void showExit();

    void showHelp();

    void showSystemInfo();

    void showVersion();

    void showWelcome();

}
