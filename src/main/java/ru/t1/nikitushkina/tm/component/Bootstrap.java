package ru.t1.nikitushkina.tm.component;

import ru.t1.nikitushkina.tm.api.controller.ICommandController;
import ru.t1.nikitushkina.tm.api.controller.IProjectController;
import ru.t1.nikitushkina.tm.api.controller.IProjectTaskController;
import ru.t1.nikitushkina.tm.api.controller.ITaskController;
import ru.t1.nikitushkina.tm.api.repository.ICommandRepository;
import ru.t1.nikitushkina.tm.api.repository.IProjectRepository;
import ru.t1.nikitushkina.tm.api.repository.ITaskRepository;
import ru.t1.nikitushkina.tm.api.service.ICommandService;
import ru.t1.nikitushkina.tm.api.service.IProjectService;
import ru.t1.nikitushkina.tm.api.service.IProjectTaskService;
import ru.t1.nikitushkina.tm.api.service.ITaskService;
import ru.t1.nikitushkina.tm.constant.ArgumentConst;
import ru.t1.nikitushkina.tm.constant.CommandConst;
import ru.t1.nikitushkina.tm.controller.CommandController;
import ru.t1.nikitushkina.tm.controller.ProjectController;
import ru.t1.nikitushkina.tm.controller.ProjectTaskController;
import ru.t1.nikitushkina.tm.controller.TaskController;
import ru.t1.nikitushkina.tm.enumerated.Status;
import ru.t1.nikitushkina.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.nikitushkina.tm.exception.system.CommandNotSupportedException;
import ru.t1.nikitushkina.tm.model.Project;
import ru.t1.nikitushkina.tm.repository.CommandRepository;
import ru.t1.nikitushkina.tm.repository.ProjectRepository;
import ru.t1.nikitushkina.tm.repository.TaskRepository;
import ru.t1.nikitushkina.tm.service.CommandService;
import ru.t1.nikitushkina.tm.service.ProjectService;
import ru.t1.nikitushkina.tm.service.ProjectTaskService;
import ru.t1.nikitushkina.tm.service.TaskService;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);
    private final ICommandController commandController = new CommandController(commandService);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);
    private final ITaskService taskService = new TaskService(taskRepository);
    private final ITaskController taskController = new TaskController(taskService);

    void initDemoData() {
        projectService.add(new Project("Project 02", "p02", Status.NOT_STARTED));
        projectService.add(new Project("Project 03", "p03", Status.COMPLETED));
        projectService.add(new Project("Project 01", "p01", Status.IN_PROGRESS));

        taskService.create("Task 03", "t03");
        taskService.create("Task 01", "t01");
        taskService.create("Task 02", "t02");
    }

    private void processArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument) {
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            default:
                throw new ArgumentNotSupportedException(argument);
        }
        System.exit(0);
    }

    private void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String argument = args[0];
        processArgument(argument);
    }

    private void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.INFO:
                commandController.showSystemInfo();
                break;
            case CommandConst.EXIT:
                commandController.showExit();
                break;
            case CommandConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case CommandConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case CommandConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case CommandConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CommandConst.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case CommandConst.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case CommandConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case CommandConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case CommandConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case CommandConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case CommandConst.PROJECT_COMPLETE_BY_ID:
                projectController.competeProjectById();
                break;
            case CommandConst.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case CommandConst.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConst.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConst.TASK_CLEAR:
                taskController.clearTask();
                break;
            case CommandConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case CommandConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case CommandConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case CommandConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case CommandConst.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case CommandConst.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case CommandConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case CommandConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case CommandConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case CommandConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case CommandConst.TASK_COMPLETE_BY_ID:
                taskController.competeTaskById();
                break;
            case CommandConst.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case CommandConst.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case CommandConst.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case CommandConst.TASK_SHOW_BY_PROJECT_ID:
                taskController.showTaskByProjectId();
                break;
            default:
                throw new CommandNotSupportedException(command);
        }
    }

    public void run(final String[] args) {
        if (args.length > 0) processArguments(args);
        initDemoData();
        commandController.showWelcome();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
            } catch (final Exception e) {
                System.out.println(e.getMessage());
                System.out.println("[FAIL]");
            }
        }
    }

}
